﻿using DG.Tweening;
using PathologicalGames;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;


    public Transform text_pop;
    public GameObject settingpop;
    private SpawnPool myPool;
    public GameObject broken_prefeb;


    private void Awake()
    {
        Instance = this;
        myPool = GetComponent<SpawnPool>();
    }

    public void ShowPerfect()
    {
        StartCoroutine(Pop());
    }

    public void BrokenParts(Transform trans)
    {
        Transform obj = myPool.Spawn(broken_prefeb, trans.position,trans.rotation);
        trans.parent.gameObject.SetActive(false);
        StartCoroutine(WaitandDo(3,()=>
        {
            myPool.Despawn(obj);
        }));
        Player.instance.mysensor.Pulse();
        foreach (GameObject item in Player.instance.mysensor.DetectedObjects)
        {
            StartCoroutine(PullObject(item));
        }

        Time.timeScale = 0.5f;
        StartCoroutine(WaitandDo(0.75f, () =>
        {
            Time.timeScale = 1;
        }));
    }



    IEnumerator PullObject(GameObject obj)
    {
        yield return null;

        while (obj != null)
        {
           obj.transform.position = Vector3.MoveTowards(obj.transform.position, Player.instance.transform.position, Time.deltaTime * 20);
           yield return null;
        }
        
       
    }
    IEnumerator Pop()
    {
        text_pop.localScale = Vector3.zero;
        text_pop.gameObject.SetActive(true);
        text_pop.DOScale(Vector3.one,1);
        yield return new WaitForSeconds(1.2f);
        text_pop.DOScale(Vector3.zero, 0.5f);
        yield return new WaitForSeconds(0.6f);
        text_pop.gameObject.SetActive(false);
    }

    IEnumerator WaitandDo(float wait,Action evnt)
    {
        yield return new WaitForSeconds(wait);
        evnt.Invoke();
    }
}
