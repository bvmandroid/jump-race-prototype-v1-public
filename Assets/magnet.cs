﻿using SensorToolkit;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class magnet : MonoBehaviour
{

    public RangeSensor myRange;
    public float forc;


    private void Update()
    {
        transform.Rotate(Vector3.right * Time.deltaTime * 350);
        transform.Rotate(Vector3.up * Time.deltaTime * 350);
    }
    void FixedUpdate()
    {
        if (myRange.DetectedObjects.Count !=0)
        {
            Player.instance.rb.AddForce((transform.position- Player.instance.transform.position).normalized* forc,ForceMode.Acceleration);
        }
    }
}
