﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrokenScript : MonoBehaviour
{
    public List<Breakinfo> myInfo = new List<Breakinfo>();
    
    
    [Button]
    void Load()
    {
        Debug.Log(transform.childCount);
        myInfo = new List<Breakinfo>();
        foreach (Transform item in transform)
        {
            myInfo.Add(new Breakinfo
            {
                rb = item.GetComponent<Rigidbody>(),
                trans = item,
                posi = item.localPosition,
                scale = item.localScale,
                angle = item.localEulerAngles
            });
        }        
    }


    void OnSpawned()
    {        
        foreach (Breakinfo item in myInfo)
        {
            item.rb.isKinematic = false;
            item.rb.velocity = new Vector3(Random.Range(0.5f,3f), Random.Range(0.5f, 20f), Random.Range(0.5f, 3f));
        }
    }


    void OnDespawned()
    {
        foreach (Breakinfo item in myInfo)
        {
            item.rb.isKinematic = true;
            item.rb.velocity = Vector3.zero;
            item.rb.angularVelocity = Vector3.zero;

            item.trans.localScale = item.scale;
            item.trans.localEulerAngles = item.angle;
            item.trans.localPosition = item.posi;

        }
    }
    
}


[System.Serializable]
public class Breakinfo
{
    public Rigidbody rb;
    public Transform trans;
    public Vector3 posi;
    public Vector3 angle;
    public Vector3 scale;
}
