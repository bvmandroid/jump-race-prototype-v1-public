﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class cloudmanager : MonoBehaviour
{
    public Image myClouds;
    public int maxHight = 300;


    public float debu;
    

    // Update is called once per frame
    void Update()
    {        
        var tempColor = myClouds.color;
        var val = Mathf.Clamp(Player.instance.transform.position.y,0.1f,0.85f);
        debu = val;
        tempColor.a = Player.instance.transform.position.y/300;
        myClouds.color = tempColor;
    }
}
