﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Killer : MonoBehaviour
{

    public GameObject fire;
    public Collider mycol;

    private void Start()
    {
        StartCoroutine(Loop());
    }



    [Button]
    private void ali()
    {
        transform.eulerAngles = Vector3.zero;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 10)
        {
            Player.instance.Restart();
        }
    }

    IEnumerator Loop()
    {
        while (true)
        {
            yield return new WaitForSeconds(5);
            fire.SetActive(true);
            mycol.enabled = true;
            yield return new WaitForSeconds(5);
            fire.SetActive(false);
            mycol.enabled = false;
        }
    }
}
