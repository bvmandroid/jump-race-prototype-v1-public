﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerScript : MonoBehaviour
{
    public int layermask;
    public UnityEvent myTrigger;


    private void OnTriggerEnter(Collider other)
    {
        
        //Debug.Log("Perfect");
        if(other.gameObject.layer == layermask) myTrigger.Invoke();
        this.gameObject.SetActive(false);
    }
}
