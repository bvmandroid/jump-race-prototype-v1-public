﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class FollowPlayer : MonoBehaviour
{
    public Transform target;
    public Vector3 padding;
    public float speed;

    private void Update()
    {
        transform.position = Vector3.Lerp(transform.position, target.position,Time.deltaTime*speed);
        transform.eulerAngles = target.eulerAngles;
    }
}
