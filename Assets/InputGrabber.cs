﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputGrabber : MonoBehaviour
{

    public float val = 0;

    private void OnMouseDrag()
    {
        val = Input.GetAxis("Horizontal");
    }
}
