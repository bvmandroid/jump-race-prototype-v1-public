﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Jumper : MonoBehaviour
{

    public float rightLimit = 2.5f;
    public float leftLimit = 1.0f;
    public float speed = 2.0f;
    private int direction = 1;


    void Start()
    {
       if (transform.eulerAngles != Vector3.zero ) transform.eulerAngles = Vector3.zero;
    }


    void Update()
    {
        if (transform.localPosition.x > rightLimit)
        {
            direction = -1;
        }
        else if (transform.localPosition.x < leftLimit)
        {
            direction = 1;
        }
        var movement = Vector3.right * direction * speed * Time.deltaTime;
        transform.Translate(movement);
    }

    public void Perfect()
    {
        GameManager.Instance.ShowPerfect();
        GameManager.Instance.BrokenParts(transform);
        Player.instance.Jump(7);
    }

}
