﻿using DG.Tweening;
using Dreamteck.Splines;
using SensorToolkit;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public static Player instance;

    public bool rotateCam = false;
    public float Progress = 0;
    public bool canMove = true;
   // public int jumpSpeed = 50;
   // public int moveSpeed = 50;
    public float rotateSpeed = 1;
    public Rigidbody rb;
    public LayerMask jump;
    public SplineProjector spline_project;
    public Slider progress_ui;
    public TextMeshProUGUI progress_percent_ui;
    public LineRenderer line;
    public Material line_mat;
    public Vector2 Drag;
    public Transform target;
    public Animator anim;
    public Transform cam;
    private Vector2 mouseDown_previousFrame = Vector2.zero;
    RaycastHit hit;
    public GameObject rest_btn;
    public RangeSensor mysensor;

    public Slider speed_slid,jump_slid;
    public TextMeshProUGUI speed_show, jump_show;

    private void Awake()
    {
        instance = this;
        rb = GetComponent<Rigidbody>();
        Physics.gravity = new Vector3(0,-15,0);
    }

    private void Update()
    {
       
        progress_ui.value = (float)spline_project.result.percent;
        progress_percent_ui.text = (progress_ui.value*100).ToString("F2") + "%";
        if (!GameManager.Instance.settingpop.activeInHierarchy && canMove && Input.GetMouseButton(0) && mouseDown_previousFrame!= Vector2.zero)
        {
            Drag = mouseDown_previousFrame - (Vector2)Input.mousePosition;
            Drag *= -1;
            // Drag *= -1;
            // var rot = Drag.x * rotateSpeed;
            // rot = Mathf.Clamp(rot,-1.5f,1.5f);
            transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + Drag.x*rotateSpeed, 0);
            // mouseDown_previousFrame= (Vector2)Input.mousePosition;
            mouseDown_previousFrame = Input.mousePosition;
        }

        line.SetPosition(0, transform.position);
        if (Physics.Raycast(transform.position, -transform.up, out hit, Mathf.Infinity, jump))
        {
            line.SetPosition(1, hit.point);
            line_mat.color = Color.green;
            target.transform.position = hit.point;
        }
        else
        {
            line_mat.color = Color.red;
            target.transform.position = new Vector3(transform.position.x, 0, transform.position.z);
            line.SetPosition(1, target.transform.position);
        }

        if (transform.eulerAngles.z != 0 || transform.eulerAngles.x != 0) transform.eulerAngles = new Vector3(0, transform.eulerAngles.y, 0);
        if(canMove) if (Input.GetMouseButton(0)) Move();

        if (rotateCam) cam.localEulerAngles = new Vector3(0, cam.localEulerAngles.y+Time.deltaTime*20, 0);
       

        if (Input.GetMouseButtonDown(0)) mouseDown_previousFrame = Input.mousePosition;
        if (Input.GetMouseButtonUp(0)) mouseDown_previousFrame = Vector2.zero;

        speed_show.text = "Speed " + speed_slid.value;
        jump_show.text = "Jump " + jump_slid.value;



    }

    private void FixedUpdate()
    {
        rb.angularVelocity = Vector3.zero;
        


    }
    private void Move()
    {
        if (GameManager.Instance.settingpop.activeInHierarchy) return;
        transform.position += transform.forward * Time.deltaTime * speed_slid.value;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.layer == 8)
        {
            if (collision.transform.parent.parent.gameObject.name == "Floor")
            {
                Jump(50);
            }
            else
            {
                Jump(collision);
            }
        }
        else if (collision.gameObject.name == "Ground")
        {
            Restart();
        }
        else if (collision.gameObject.layer == 9)
        {

            rotateCam = true;
            cam.transform.localPosition = new Vector3(0, -5, 0);
            target.gameObject.SetActive(false);
            canMove = false;
            anim.SetTrigger("Dance");
            rest_btn.SetActive(true);
        }
    }
    public void Jump(Collision collision)
    {
        //collision.transform.DOShakePosition(0.5f, .7f);
        rb.velocity = new Vector3(0, jump_slid.value, 0);
        anim.SetTrigger("Jump");
    }
    public void Jump(int extra)
    {
        rb.velocity = new Vector3(0, jump_slid.value + extra, 0);
        anim.SetTrigger("Jump");
    }

    public void Restart()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
}
